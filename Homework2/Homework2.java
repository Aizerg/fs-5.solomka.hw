import java.util.Random;
import java.util.Scanner;

public class Homework2 {
    public static void main(String[] args) {

        Random random = new Random();

        // Генерирую  рандомную цель
        int randomLine = random.nextInt(1, 6); // [0-5)
        int randomColumn = random.nextInt(1, 6); // [0-5)

        // Создаю масив 5х5
        String[][] squareMassive = new String[6][6];

        //Заполняю массив значением "-"
        for (int i = 0; i < squareMassive.length; i++) {
            for (int j = 0; j < squareMassive[i].length; j++) {
                if (i == 0) {
                    squareMassive[i][j] = String.valueOf(j);
                } else if ( j == 0) {
                    squareMassive[i][j] = String.valueOf(i);
                } else {
                    squareMassive[i][j] = "-";
                }

            }
        }

        System.out.println("All set. Get ready to rumble!");
        Scanner scan = new Scanner(System.in);
        int lineOfMassive;
        int columnOfMassive;

        while (true) {
            do {
                System.out.println("Enter the number of line for shooting.");
                lineOfMassive = scan.nextInt();
                if (lineOfMassive > 5 || lineOfMassive < 1) {
                    System.err.println("You need entered the number from 1 to 5! Try again.");
                }
            } while (lineOfMassive > 5 || lineOfMassive < 1);

            do {
                System.out.println("Enter the number of column for shooting.");
                columnOfMassive = scan.nextInt();
                if (columnOfMassive > 5 || columnOfMassive < 1) {
                    System.err.println("You need entered the number from 1 to 5! Try again.");
                }
            } while (columnOfMassive > 5 || columnOfMassive < 1);

            if (randomLine == lineOfMassive && randomColumn == columnOfMassive) {
                squareMassive[lineOfMassive][columnOfMassive] = "x";
                System.out.println("You have won!");
                for (String[] line : squareMassive) {
                    System.out.print("| ");
                    for (int i = 0; i < squareMassive.length; i++) {
                        System.out.print(line[i]);
                        if (i < squareMassive.length - 1) {
                            System.out.print(" | ");
                        }
                    }
                    System.out.println(" |");
                }
                System.exit(0);
            } else {
                squareMassive[lineOfMassive][columnOfMassive] = "*";
            }

            // Вывод масива
            for (String[] line : squareMassive) {
                System.out.print("| ");
                for (int i = 0; i < squareMassive.length; i++) {
                    System.out.print(line[i]);
                    if (i < squareMassive.length - 1) {
                        System.out.print(" | ");
                    }
                }
                System.out.println(" |");
            }
        }


    }
}
