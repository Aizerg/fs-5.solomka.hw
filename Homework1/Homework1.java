import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Homework1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        // начало игры
        System.out.println("Let the game begin!");

        // Вводим свое имя и записываем в переменную name
        System.out.println("Enter your name!");
        String name = scan.nextLine();
        System.out.println(name);

        // Генерируем рандомное число от 0 до 100
        Random random = new Random();
        String[][] yearAndAction = {{"1914","1939","1991"},{"When the World War I begin?","When the World War II begin?","When the 3 action begin?"}};
        int randomColumn = random.nextInt(yearAndAction.length + 1);
        int guessNumber;
        int parseToInt = Integer.parseInt(yearAndAction[0][randomColumn]);

        System.out.println(yearAndAction[1][randomColumn]);
        int[] guessingNumbers = new int[0];

        while (true) {

            while (!scan.hasNextInt()) {
                System.err.println("You entered not a number! Please, enter a number!");
                scan.nextInt();
            }
            guessNumber = scan.nextInt();

            guessingNumbers = Arrays.copyOf(guessingNumbers, guessingNumbers.length + 1);
            guessingNumbers[guessingNumbers.length - 1] = guessNumber;

            if (guessNumber < parseToInt) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (guessNumber > parseToInt) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.printf("Congratulations, %s!\nYour numbers: ", name);
                Arrays.sort(guessingNumbers);
                System.out.println(Arrays.toString(guessingNumbers));
                break;
            }
        }
    }
}
