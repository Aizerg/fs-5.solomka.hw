package controller;

import model.*;
import service.FamilyService;
import service.InterfaceFamilyService;

import java.util.List;

public class FamilyController implements InterfaceFamilyService {
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    @Override
    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    @Override
    public void getFamiliesBiggerThan(int members) {
         familyService.getFamiliesBiggerThan(members);
    }

    @Override
    public void getFamiliesLessThan(int members) {
        familyService.getFamiliesLessThan(members);
    }

    @Override
    public void countFamiliesWithMemberNumber(int members) {
        familyService.countFamiliesWithMemberNumber(members);
    }

    @Override
    public void createNewFamily(Man father, Woman mother) {
        familyService.createNewFamily(father, mother);
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    @Override
    public Family bornChild(Family family, String menName, String womanName) {
        return familyService.bornChild(family, menName, womanName);
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    @Override
    public boolean deleteAllChildrenOlderThen(int age) {
        return familyService.deleteAllChildrenOlderThen(age);
    }

    @Override
    public int count() {
        return familyService.count();
    }

    @Override
    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    @Override
    public List<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    @Override
    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }
}
