package dao;

import model.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class FamilyTest {

    @Test
    public void testEmptyHumanToString() {
        Human human = new Human();
        assertEquals("Human{name='null', surname='null', year=0, iq=0, schedule=null}", human.toString());
    }

    @Test
    public void testHumanToString() {
        Human human = new Human("Віталій", "Соломка", 1995);
        assertEquals("Human{name='Віталій', surname='Соломка', year=1995, iq=0, schedule=null}", human.toString());
    }

    @Test
    public void testChoisenPetToString() {
        Dog pet = new Dog("Принц");
        assertEquals("DOG{nickname='Принц', age=0, trickLevel=0, habits=null}", pet.toString());
    }

    @Test
    public void testFullPetToString() {
        Dog dog = new Dog("Боня", 5, 55, new HashSet<String>(List.of("Їсть", "гуляє", "спить")));
        assertEquals("DOG{nickname='Боня', age=5, trickLevel=55, habits=[Їсть, спить, гуляє]}", dog.toString());
    }

    @Test
    public void testDeleteChild() {
        Woman mother = new Woman();
        Man father = new Man();

        Family family = new Family(mother, father);

        Human newChild = new Human();

        family.addChild(newChild);

        List<Human> emptyChild = new ArrayList<>(){};

        family.deleteChild(newChild);

        assertEquals(emptyChild, family.getChildren());

    }

    @Test
    public void testDeleteExternalChild() {
        Woman mother = new Woman("Mother", "Surname", 1993);
        Man father = new Man("Father", "Surname", 1991);

        Family family = new Family(mother, father);

        Human child = new Human("Віталій", "Соломка", 1995);
        Human externalChild = new Human("External", "child", 1993);

        family.addChild(child);
        family.deleteChild(externalChild);
        assertEquals("[Human{name='Віталій', surname='Соломка', year=1995, iq=0, schedule=null}]", family.getChildren().toString());
    }

    @Test
    public void testDeleteChildFromMassive() {
        Woman mother = new Woman("Mother", "Surname", 1993);
        Man father = new Man("Father", "Surname", 1991);

        Family family = new Family(mother, father);
        Family familyWithoutChildren = new Family(mother, father);

        Human child = new Human("Віталій", "Соломка", 1995);

        int oldFamilyLenght = family.getChildren().size();

        family.addChild(child);
        family.deleteChild(child);

        int newFamilyLenght = family.getChildren().size();

        assertEquals(oldFamilyLenght, newFamilyLenght);
        assertEquals(familyWithoutChildren.getChildren(), family.getChildren());
    }

    @Test
    public void testCountFamily() {
        Woman mother = new Woman("Mother", "Surname", 1993);
        Man father = new Man("Father", "Surname", 1991);

        Family family = new Family(mother, father);

        Human child = new Human("Віталій", "Соломка", 1995);
        Human child1 = new Human("Віталій1", "Соломка1", 1991);

        family.addChild(child);
        family.addChild(child1);

        assertEquals(2, family.getChildren().size());
    }

}
