package dao;

import model.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> allFamilies = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return allFamilies;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return allFamilies.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (allFamilies.size() <= index || index < 1) {
            allFamilies.remove(index);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (allFamilies.contains(family)) {
            allFamilies.remove(family);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void saveFamily(Family family) {
        if (allFamilies.contains(family)) {
            allFamilies.set(allFamilies.indexOf(family), family);
        } else {
            allFamilies.add(family);
        }

    }
}
