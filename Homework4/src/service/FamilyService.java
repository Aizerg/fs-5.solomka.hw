package service;

import dao.FamilyDao;
import model.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FamilyService implements InterfaceFamilyService {
    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    @Override
    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    @Override
    public void displayAllFamilies() {
        System.out.println(familyDao.getAllFamilies());
    }

    @Override
    public void getFamiliesBiggerThan(int members) {
        final List<Family> allFamilies = getAllFamilies();
        final List<Family> sortedFamilies = new ArrayList<>();
        for (Family family : allFamilies) {
            if (family.countFamily() > members) {
                sortedFamilies.add(family);
            }
        }
        System.out.println(sortedFamilies);
    }

    @Override
    public void getFamiliesLessThan(int members) {
        final List<Family> allFamilies = getAllFamilies();
        final List<Family> sortedFamilies = new ArrayList<>();
        for (Family family : allFamilies) {
            if (family.countFamily() < members) {
                sortedFamilies.add(family);
            }
        }
        System.out.println(sortedFamilies);;
    }

    @Override
    public void countFamiliesWithMemberNumber(int members) {
        final List<Family> allFamilies = getAllFamilies();
        int count = 0;
        for (Family family : allFamilies) {
            if (family.countFamily() == members) {
                count++;
            }
        }
        System.out.println(count);;
    }

    @Override
    public void createNewFamily(Man father, Woman mother) {
        Family newFamily = new Family(mother, father);
        familyDao.getAllFamilies().add(newFamily);
    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        if (index < familyDao.getAllFamilies().size() && index < 0) {
            familyDao.getAllFamilies().remove(index);
            return true;
        }
        return false;
    }

    @Override
    public Family bornChild(Family family, String menName, String womanName) {
        Random random = new Random();
        int randomInt = random.nextInt(2);
        if (randomInt == 1) {
            Human child = new Human(menName, family.getFather().getSurname(), LocalDate.now().getYear());
            family.addChild(child);
            return family;
        } else {
            Human child = new Human(womanName, family.getFather().getSurname(), LocalDate.now().getYear());
            family.addChild(child);
            return family;
        }
    }

    @Override
    public Family adoptChild(Family family, Human child) {
        familyDao.getFamilyByIndex(familyDao.getAllFamilies().indexOf(family)).addChild(child);
        return family;
    }

    @Override
    public boolean deleteAllChildrenOlderThen(int age) {
        List<Family> allFamilies = getAllFamilies();
        for (Family family: allFamilies
             ) {
          List<Human> children =  family.getChildren();
            for (Human child: children
                 ) {
                if(child.getYear() > age) {
                    family.deleteChild(child);
                }
            }

        }
        return true;
    }

    @Override
    public int count() {
        return familyDao.getAllFamilies().size();
    }

    @Override
    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    @Override
    public List<Pet> getPets(int familyIndex) {
       return familyDao.getFamilyByIndex(familyIndex).getPets();
    }

    @Override
    public void addPet(int familyIndex, Pet pet) {
        familyDao.getFamilyByIndex(familyIndex).setPet(pet);
    }
}
