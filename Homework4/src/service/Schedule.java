package service;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Schedule {
    private final Map<DayOfWeek, ArrayList<String>> schedule = new HashMap<>();

    private final DayOfWeek[] days = new DayOfWeek[]{DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY,
            DayOfWeek.FRIDAY, DayOfWeek.SATURDAY, DayOfWeek.SUNDAY};

    public Map<DayOfWeek, ArrayList<String>> getSchedule() {
        return schedule;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "schedule=" + schedule +
                '}';
    }

    String[] activitiesOfDay = new String[]{"Do HW", "Go to the gym", "Do sport", "Do something", "Do nothing", "Go walk", "Go to the parents"};

    public void addDefaultActivities () {
        for (int i = 0; i < days.length; i++) {
            schedule.put(days[i], new ArrayList<>(List.of(activitiesOfDay[i])));
        }
        System.out.println(schedule);
    }
}
