package service;

import model.*;

import java.util.List;

public interface InterfaceFamilyService {
    List<Family> getAllFamilies();

    void displayAllFamilies();

    void getFamiliesBiggerThan(int members);

    void getFamiliesLessThan(int members);

    void countFamiliesWithMemberNumber(int members);

    void createNewFamily(Man father, Woman mother);

    boolean deleteFamilyByIndex(int index);

    Family bornChild(Family family, String menName, String womanName);

    Family adoptChild(Family family, Human child);

    boolean deleteAllChildrenOlderThen(int age);

    int count();

    Family getFamilyById(int index);

    List<Pet> getPets(int index);

    void addPet(int familyIndex, Pet pet);
}
