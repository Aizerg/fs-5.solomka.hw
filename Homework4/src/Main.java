import controller.FamilyController;
import dao.CollectionFamilyDao;
import model.*;
import service.FamilyService;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Main {
    public static void main(String[] args) {
        Man father1 = new Man("Vitalii", "Solomka", 1995);
        Woman mother1 = new Woman("Irina", "Kotsura", 1999);

        CollectionFamilyDao collectionFamilyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.createNewFamily(father1, mother1);

        familyController.displayAllFamilies();

        familyController.getFamilyById(0);

        System.out.println(familyController.getAllFamilies());
        familyController.addPet(0, new DomesticCat("Bonya"));

        familyController.adoptChild(familyController.getFamilyById(0), new Human("Adopt", "Child", 2001));

        System.out.println(familyController.count());

        familyController.bornChild(familyController.getFamilyById(0), "Denis", "Mariya");

        familyController.countFamiliesWithMemberNumber(2);
        familyController.countFamiliesWithMemberNumber(4);

        familyController.getFamiliesLessThan(5);

        familyController.deleteAllChildrenOlderThen(1);

        familyController.addPet(0, new DomesticCat("Ryjuk"));

        System.out.println(familyController.getPets(0));


//        for (int i = 0; i < 1000000; i++) {
//            new Human();
//        }

    }

}
