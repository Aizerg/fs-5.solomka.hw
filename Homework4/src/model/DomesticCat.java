package model;

import model.Family;
import model.Pet;
import service.Foul;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {
    private final Species species = Species.DOMESTIC_CAT;

    public DomesticCat() {

    }

    public DomesticCat(String nickname) {
        super(nickname);
    }


    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void foul() {
        System.out.println("Я заплутавля в шторі, будь-ласка можете визволити мене!)))");
    }

    @Override
    public void respond() {
        System.out.printf("Привіт, хазяїн. Я - домашній кіт, мене звати %s. Я скучив, а ще я хочу їсти завджи!\n", super.getNickname());
    }

    @Override
    public Species getSpecies() {
        return species;
    }

}
