package model;

import model.Family;

import java.util.Set;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set <String> habits;

    private Family family;

    public void eat() {
        System.out.println("Я ї'м!");
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public abstract void respond();

    Pet() {

    }

    Pet(String nickname) {
        this.nickname = nickname;
    }


    public Pet(String nickname, int age, int trickLevel, Set<String> habits, Family family) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
        this.family = family;
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }


    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }


    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return getHabits();
    }

    public void setHabits() {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return getSpecies() == null ? "Pet" + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}' : getSpecies().name() + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }


    @Override
    public void finalize() {
        System.out.println("deleting : ");
        System.out.println(this);
    }

}