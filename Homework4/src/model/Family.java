package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Family {
    private final Woman mother;
    private final Man father;
    private List<Human> children;
    private List<Pet> pets = new ArrayList<>();;

    public Family(Woman mother, Man father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.children = new ArrayList<>(){};
    }

    public Family(Woman mother, Man father, List<Pet> pets) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
        this.pets = pets;
        this.children = new ArrayList<>(){};
    }

    public Woman getMother() {
        return mother;
    }

    public Man getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public List<Pet> getPets() {
        return pets;
    }
    public Pet getPet(int index){ return pets.get(index);}
    public Pet getPet(){ Random random = new Random(); return pets.get(random.nextInt(pets.size()));}

    public void setPet(Pet pet) {
        pets.add(pet);
        pet.setFamily(this);
    }

    public void addChild(Human child) {
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(Human child){
        List<Human> childrenList = children;
        if(child.getFamily() == null || !childrenList.contains(child)){
            return false;
        }
        children.remove(child);
        return true;
    }

    public boolean deleteChild(int index) {
        if (index >= children.size() || index < 0) {
            return false;
        }
        children.remove(index);
        return true;
    }


    public int countFamily() {
        return (2 + children.size());
    }



    @Override
    public String toString() {
        return "model.Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pets=" + pets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!mother.equals(family.mother)) return false;
        if (!father.equals(family.father)) return false;
        return children.equals(family.getChildren());
    }

    @Override
    public void finalize() {
        System.out.println("deleting : ");
        System.out.println(this);
    }

    @Override
    public int hashCode() {
        int result = mother.hashCode();
        result = 31 * result + father.hashCode();
        result = 31 * result + children.hashCode();
        return result;
    }


}
