package model;

import model.Human;

import java.util.ArrayList;
import java.util.Map;

public final class Man extends Human {
    public Man() {

    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    Man(String name, String surname, int year, int iq, Map<DayOfWeek, ArrayList<String>> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void repairCar () {
        System.out.println("Масло поміняв, шини нові поставил, можна їздити!");
    }
    @Override
    public void greetPet() {
        System.out.printf("Привіт, %s\n", getFamily().getPet().getNickname());
    }

}
