package model;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

public class Human {
    private static Family family;
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Map<DayOfWeek, ArrayList<String>> schedule;


    public void greetPet() {
        System.out.printf("Привіт, %s\n", family.getPet().getNickname());
    }

    public static void describePet() {
        String trickLevelString = "";
        if (family.getPet().getTrickLevel() > 50 && family.getPet().getTrickLevel() <= 100) {
            trickLevelString = "дуже хитрий";
        } else if (family.getPet().getTrickLevel() >= 0 && family.getPet().getTrickLevel() <= 50) {
            trickLevelString = "дуже хитрий";
        }
        System.out.printf("У мене є %s, їй %d років, він %s\n", family.getPet().getSpecies(), family.getPet().getAge(), trickLevelString);
    }

    public Human() {

    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    Human(String name, String surname, int year, int iq, Map<DayOfWeek, ArrayList<String>> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        Human.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, ArrayList<String>> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<DayOfWeek, ArrayList<String>> schedule) {
        this.schedule = schedule;
    }

//    @Override

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return getFamily().hashCode();
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public void finalize() {
        System.out.println("Data is deleting: ");
        System.out.println(this);
    }

}