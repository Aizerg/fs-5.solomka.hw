package model;

import model.Family;
import service.Foul;

import java.util.Set;

public class RoboCat extends Pet implements Foul {
    private final Species species = Species.ROBO_CAT;

    RoboCat() {

    }

    RoboCat(String nickname) {
        super(nickname);
    }


    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void foul() {
        System.out.println("Ця ваза лишня в нашому будинку - тому я її прибрала зі стола!");
    }

    @Override
    public void respond() {
        System.out.printf("При-віт, ха-зяїн. Я - Робо-Кот. Мене зовуть %s. Что прикажите?\n", super.getNickname());
    }

    @Override
    public Species getSpecies() {
        return species;
    }

}
