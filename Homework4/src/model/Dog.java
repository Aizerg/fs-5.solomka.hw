package model;

import model.Family;
import model.Pet;
import service.Foul;

import java.util.Set;

public class Dog extends Pet implements Foul {
    private final Species species = Species.DOG;

    Dog() {

    }

    public Dog(String nickname) {
        super(nickname);
    }


    public Dog(String nickname, int age, int trickLevel, Set<String> habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void foul() {
        System.out.println("Я випадково погриз ваші кросівки... Треба якнайшвидше сховатись!");
    }

    @Override
    public void respond() {
        System.out.printf("Привіт, хазяїн. Я - собака, мене звати %s. Я скучив!\n", super.getNickname());
    }

    @Override
    public Species getSpecies() {
        return species;
    }

}
