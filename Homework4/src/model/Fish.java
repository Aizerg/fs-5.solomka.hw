package model;

import model.Family;
import model.Pet;

import java.util.Set;

public class Fish extends Pet {
    private final Species species = Species.FISH;

    Fish() {

    }

    Fish(String nickname) {
        super(nickname);
    }


    public Fish(String nickname, int age, int trickLevel, Set<String> habits, Family family) {
        super(nickname, age, trickLevel, habits, family);
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf("Буль-буль! Я - рибка %s. Я сьогодні плаваю цілий день!\n", super.getNickname());
    }

    @Override
    public Species getSpecies() {
        return species;
    }

}
