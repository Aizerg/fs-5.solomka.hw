package model;

import model.Human;

import java.util.ArrayList;
import java.util.Map;

public final class Woman extends Human {
    public Woman() {

    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    Woman(String name, String surname, int year, int iq, Map<DayOfWeek, ArrayList<String>> schedule) {
        super(name, surname, year, iq, schedule);
    }
    @Override
    public void greetPet() {
        System.out.printf("Привіт, %s\n", super.getFamily().getPet().getNickname());
    }

    public void makeup () {
        System.out.println("Я нафарбувалась, можем виходити!");
    }

}

